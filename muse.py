#!/usr/bin/python3

import sys
import os
import configparser
import argparse
import datetime

def main():
    
    config_path = os.path.expanduser("~/.config/muse.cfg")

    parser = argparse.ArgumentParser(description="Make a log")
    parser.add_argument('-l', action="store_true")
    r = parser.parse_args()
    
   
    config = configparser.ConfigParser()

    if os.path.isfile(config_path):
        config.read(config_path)
    else:
        config.add_section('default')
        config.set('default', 'log_file', os.path.expanduser("~/.config/musings"))
        with open(config_path, 'w') as cfg: config.write(cfg)
    
    location = config['default']['log_file']
    
    if not os.path.isfile(location): 
        print("Enter location for log file (~/.config/musings): ",end='')
        location = os.path.expanduser(input())
        location = location if location else os.path.expanduser("~/.config/musings")
        config["log_file"] = location
        config.write(config_path)

        with open(location, 'w') as lf: pass
    
    if not r.l:
        with open(location, 'a+') as lf:
            thot = input("Whats up?> ")
            s = f"\n<{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}>: {thot}"
            lf.write(s)
            print(s)
        return

    else:
        with open(location, 'r+') as lf:
            print(lf.read())


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
